<div class="row-fluid">
    
    <?php
    //Cette vue est la page d'accueil
    //La méthode renderPartial inclut les vues partielles.
    //Ces vues partielles sont situées dans /protected/views/site/zones
    //Si jamais on veut passer des data (fournies par le contrôleur) aux vues partielles : 
    //$this->renderPartial('zones/zone1', array('data1'=>'valeurData1', 'data2'=>'valeurData2'));
    
    if(!Yii::app()->user->isGuest)
        $this->renderPartial('zones/zone1/main',array('messons'=>$messons, 'musiquesfavoris'=>$musiquesfavoris, 'posts'=>$posts));  

    $this->renderPartial('zones/zone2', array('musiquesrecentes'=>$musiquesrecentes, 'recommandations'=>$recommandations));
    $this->renderPartial('zones/zone3');

    ?>
</div>

<script type="text/javascript">


    $("#d1").css("left",$("li.item-test").first().offset().left)+"px";
    $("#d1").css("width",$("li.item-test").first().width())+5+"px";

    $("li.item-test").hover(function()
    {
        var thisitem = $(this);
        $("#d1").css("left", thisitem.offset().left);
        $("#d1").css("width", thisitem.width());
    });
    
    function add_scrolls()
    {
        
        $('div').each(function(){
            if($(this).prop('offsetHeight') < $(this).prop('scrollHeight') && $(this).css("overflow")!="hidden")
            {
                $(this).mCustomScrollbar({scrollInertia: 10});
            }
            
        });
    }
    
    
    $('input#yt0').addClass("btn-primary");
    $('#tableposts').find("div").load(function(){
        
        var $this = $(this);
        var sum=20;
        $this.children().each(function(){
            if ($(this).is("div"))
            {
                $this = $(this);
            }
            sum +=parseInt($(this).css("height")); 
        });
        if (parseInt($this.css("height"))>sum)
        {
            $this.css("max-height", ($("#content").height()));

                $this.css("overflow", "hidden");
                $this.mCustomScrollbar({scrollInertia: 10});
        }
    });
    function swapmenus()
    {
        if($(window).width()<1150)
        {
            $("#smallmenu").css("visibility","hidden");
            $("#smallmenu").css("display","inline-grid");
            $("#zonemenu").width($("#smallmenu").width());
            $("#smallmenu").css("display","");
            $("#smallmenu").css("visibility",""); 
            $("#d1").css("display","none");
            $("#smallmenu").css("left",$("#zonemenu").offset().left-20+"px");
            $("#d1").css("display","none");
            
        }
        else
        {
            $("#zonemenu").css("width", "");
            $("#d1").css("display","");
            $("ul#yw0 > li.item-test:nth(0)").each(function()
            {
                var thisitem = $(this);
                $("#d1").css("left", thisitem.offset().left);
                $("#d1").css("width", thisitem.width());
                $("#d1").css("display", "block");
            });
        }
    }
    $(".jp-title").prependTo("#footer");
    $(document).ready(function(){
        swapmenus();
        add_scrolls();
    });
    window.onresize = function()
    {
        swapmenus();
        add_scrolls();    
    };
</script>
