<?php if (!Yii::app()->user->isGuest) { ?>
    <?php
    if (Yii::app()->user->villeresidence != null) {
        echo CHtml::hiddenField('', Yii::app()->user->villeresidence->latitude, array('id' => 'latitude'));
        echo CHtml::hiddenField('', Yii::app()->user->villeresidence->longitude, array('id' => 'longitude'));
    }
    ?>
    <span class="top-collab">
    <h3>Si vous recherchez une groupe de musique près de chez vous, vous êtes au bon endroit.</h3>
    <p>
        <label style="display:inline">Dans quel rayon (en km) : </label><?php echo CHtml::numberField('', '0', array('min' => 0, 'id' => 'inputrayon')); ?>
    </p>
    </span>
    <div id="EmplacementDeMaCarte" style="height:100%; width:50%;"></div>
    <noscript>
    <p>Attention : </p>
    <p>Afin de pouvoir utiliser Google Maps, JavaScript doit être activé.</p>
    <p>Or, il semble que JavaScript est désactivé ou qu'il ne soit pas supporté par votre navigateur.</p>
    <p>Pour afficher Google Maps, activez JavaScript en modifiant les options de votre navigateur, puis essayez à nouveau.</p>
    </noscript>
    <script>

        function initialisation() {
            var latitude = parseFloat($('#latitude').val());
            var longitude = parseFloat($('#longitude').val());
            var rayon = parseInt($('#inputrayon').val());
            var zoom = 10;
            //Calcul du zoom auto
            if(rayon>10)
                zoom = 9;
            else if(rayon>20)
                zoom=7;
            else if(rayon >50)
                zoom=5;
            
            var optionsCarte = {
                zoom: zoom,
                center: {lat: latitude, lng: longitude},
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var maCarte = new google.maps.Map(document.getElementById("EmplacementDeMaCarte"), optionsCarte);
            var optionsCercle = {
                map: maCarte,
                center: maCarte.getCenter(),
                radius: rayon*1000//en mètres
            }
            var monCercle = new google.maps.Circle(optionsCercle);
        }

        $('#inputrayon').bind('keyup mouseup', function () {
            initialisation();
        });
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwzp0h4f8WUQwC20zccYHlNYIqTU8qyvs&callback=initialisation"></script>
<?php
}

