<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<section class="main-body">
    <div class="container" id="grandezoneblanche">
        <div id="content">
            <?php echo $content; ?>
        </div><!-- content -->
	</div>
</section>
<?php $this->endContent(); ?>