<div class="container">

    <div class="spanlogos">
        <a href="<?php echo Yii::app()->baseUrl; ?>" id="logo" class="logo"></a>
        <a href="<?php echo Yii::app()->baseUrl; ?>" id="logo2" class="logo"></a>
    </div><!--/.span6 -->
    <div class="navbar" id="navigation-mainnavbar">
        <div id="zonemenu" class="nav-collapse">
            <?php
            $collaboration = CHtml::ajaxLink(
                            'Collaboration', array('site/collaboration'), array(
                        'type' => 'POST',
                        'success' => 'function(result){ $("#content").html(result);}',
                        'error' => 'function(result){console.log(result);}'
                            ), array('data-description' => 'Des artistes à proximité !'));

            $upload = CHtml::ajaxLink(
                            'Upload', array('musique/create'), array(
                        'type' => 'POST',
                        'success' => 'function(result){ $("#content").html(result);}'
                            )
            );

            $album = CHtml::ajaxLink(
                            'Album', array('album/create'), array(
                        'type' => 'POST',
                        'success' => 'function(result){ $("#content").html(result);}',
                            )
            );

            $about = CHtml::ajaxLink(
                            'A propos', array('site/about'), array(
                        'type' => 'POST',
                        'success' => 'function(result){ $("#content").html(result);}'
                            )
            );

            $admin = CHtml::ajaxLink(
                            'Admin', array('musique/admin'), array(
                        'type' => 'POST',
                        'success' => 'function(result){ $("#content").html(result);}',
                        'error' => 'function(result){console.log(result);}'
                            )
            );
            ?>
            <ul class="menu-nav"><li class="item-menu item-menu-prime"><span><a href="#">Menus</a></span></li><li>

<?php
$this->widget('zii.widgets.CMenu', array(
    'htmlOptions' => array('class' => 'nav navtopbar', 'id' => 'smallmenu'),
    'submenuHtmlOptions' => array('class' => 'dropdown-menu'),
    'itemCssClass' => 'item-menu',
    'encodeLabel' => false,
    'items' => array(
        array('label' => $upload, 'visible' => !Yii::app()->user->isGuest),
        array('label' => $album, 'visible' => !Yii::app()->user->isGuest),
        array('label' => $collaboration),
        array('label' => $about),
        array('label' => $admin, 'visible' => Yii::app()->user->isAdmin())
    ),
));
?><li></ul>
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'htmlOptions' => array('class' => 'nav navtopbar'),
                        'submenuHtmlOptions' => array('class' => 'dropdown-menu'),
                        'itemCssClass' => 'item-test',
                        'encodeLabel' => false,
                        'items' => array(
                            array('label' => $upload, 'visible' => !Yii::app()->user->isGuest),
                            array('label' => $album, 'visible' => !Yii::app()->user->isGuest),
                            array('label' => $collaboration),
                            array('label' => $about, 'visible' => !Yii::app()->user->isAdmin()),
                            array('label' => $admin, 'visible' => Yii::app()->user->isAdmin())
                        ),
                    ));
                    ?>
            <div id="d1"></div>
        </div>

        <div id="zonerecherche" class="nav-collapse">
            <section class="webdesigntuts-workshop">
<?php
$form = $this->beginWidget('CActiveForm', array(
        //         'action' => Yii::app()->createUrl('musique/search'),
        ));
?>

                <div id="zonebarrederecherche" class="row">
                <?php
                echo CHtml::textField('recherche', '', array('name' => 'recherche', 'id' => 'search', 'placeholder' => "Recherche d'artiste ou musique"));
                if (Yii::app()->controller->id == 'site' && Yii::app()->controller->action->id == 'index') {

                    echo CHtml::ajaxSubmitButton('', CHtml::normalizeUrl(array('musique/search')), array(
                        'data' => 'js:jQuery(this).parents("form").serialize()',
                        'success' => 'function(data){
	                          $("#zone2").html(data);
	                   }',
                        'error' => 'function(data){console.log(data);}'
                            ), array(
                        'id' => 'ajaxSubmitBtn',
                        'name' => 'ajaxSubmitBtn',
                        'class' => 'btn'
                    ));
                }else
                {
                    echo CHtml::ajaxSubmitButton('', CHtml::normalizeUrl(array('musique/search')), array(
                        'data' => 'js:jQuery(this).parents("form").serialize()',
                        'success' => 'function(data){
	                          $("#content").html(data);
	                   }',
                        'error' => 'function(data){console.log(data);}'
                            ), array(
                        'id' => 'ajaxSubmitBtn',
                        'name' => 'ajaxSubmitBtn',
                        'class' => 'btn'
                    ));
                }
                ?>
                </div>

                    <?php $this->endWidget(); ?>
            </section>
        </div>
    </div>
    <div class="spanconnexion">
        <?php
        if (Yii::app()->user->isGuest) {
            ?>
            <button id="showconnect" class="btn btn-large" type="button">Connexion Inscription</button>

            <div id="espaceConnexion">
                <form action="<?php echo Yii::app()->getBaseUrl(); ?>/index.php/site/login" method="POST">
                    <label>Adresse mail : </label><input type="text" name="username"/><br/>
                    <label>Mot de passe : </label><input type="password" name="password"/><br/>
                    <input type="submit" class="btn" name="LoginForm" value="Se connecter"/>
                </form>
                <p>C'est votre première visite ? <a href="<?php echo Yii::app()->getBaseUrl(); ?>/index.php/utilisateur/create">Inscrivez-vous !</a></p>
            </div>

<?php } else {
    ?>
            <button id="showconnect" class="btn btn-large" type="button">Mon profil</button>
            <div id="espaceConnexion">
                <table>
                    <tr>
                        <td id="imgtd">
                            <img width="50px" height="50px" src="<?php echo Yii::app()->baseUrl . '/img/photosprofiles/' . Yii::app()->user->urlphoto; ?>"> <?php echo "<p class='whitep'>".Yii::app()->user->nom." ".Yii::app()->user->Prenom."</p>";?>
                        </td>
                    </tr>
                    <tr>
                        <td>
    <?php //echo CHtml::Button('Mon profil', array('submit' => Yii::app()->baseUrl . '/index.php/utilisateur/' . Yii::app()->user->idutilisateur, 'class' => 'btn', 'style' => 'width:100%; height:100%;'));   ?>
    <?php
    echo CHtml::ajaxButton('Mon profil', Yii::app()->baseUrl . '/index.php/utilisateur/' . Yii::app()->user->idutilisateur, array(
        'type' => 'POST',
        'url' => Yii::app()->baseUrl . '/index.php/utilisateur/' . Yii::app()->user->idutilisateur,
        'success' => 'function(result){ $("#content").html(result);}',
        'error' => 'function(data){console.log(data);}'
            ), array('class' => 'btn', 'style' => 'width:100%; height:100%;'));
    ?>
                            <?php echo CHtml::button('Se déconnecter', array('submit' => array('site/logout'), 'class' => 'btn', 'style' => 'width:100%; height:100%;')); ?>
                        </td>
                    </tr>
                </table>
            </div>
<?php } ?>


    </div><!--/.span6 -->
</div>