function MAJTitreMesFavoris(){
    if($('#mesfavoris table tr').length>0)
        $('#mesfavoris h3').html('Vos musiques favorites :');
    else
        $('#mesfavoris h3').html("Vous n'avez pas de musiques dans vos favoris !");
}
function ajouterLigneDansMesFavoris(ligne, idmusique) {
    var copieLigne = ligne.clone();
    copieLigne.attr('idmusique',idmusique);
    copieLigne.children().last().remove();
    copieLigne.find(".etoile").remove();
    var artiste = copieLigne.children().last().html();
    copieLigne.children().first().append(" de ");
    copieLigne.children().first().append(artiste);
    copieLigne.children().last().remove();
    $("#mesfavoris table").append(copieLigne);
    MAJTitreMesFavoris();
}

function supprimerLigneDeMesFavoris(etoile){
    var idmusique = etoile.attr("idmusique");//= $("#mesfavoris table").find("img[idmusique="++"");
    $("#mesfavoris table").find('tr[idmusique='+idmusique+']').remove();
    MAJTitreMesFavoris();
}

function gererFavori(obj) {
    if ($(obj).attr("favori") == "false") {
        $.post(
                $("#urlcomplete").val() + "/index.php/musique/ajouterenfavori",
                {idmusique: $(obj).attr('idmusique'), idutilisateur: $("#idutilisateur").val()},
                function (result) {
                    result = JSON.parse(result);
                    //alert(result.message);
                    if (result.codeErreur) {
                        //Si ça c'est bien passé, on doit mettre à jour l'image étoile de blanche à jaune
                        $(obj).attr("favori", "true");
                        $(obj).attr("src", $("#urletoilejaune").val());
                        ajouterLigneDansMesFavoris($(obj).parent().parent(), $(obj).attr('idmusique'));//On lui passe donc le tr de la musique dans "récents"
                        //et ajouter dans la tableau des favoris la musique                      
                    }
                }
        ).fail(function (xhr) {
            alert(xhr.responseText);
        });
    } else
    {

        $.post(
                $("#urlcomplete").val() + "/index.php/musique/supprimerdemesfavoris",
                {idmusique: $(obj).attr('idmusique'), idutilisateur: $("#idutilisateur").val()},
                function (result) {
                    result = JSON.parse(result);
                    //alert(result.message);
                    if (result.codeErreur) {
                        //Si ça c'est bien passé, on doit mettre à jour l'image étoile de jaune à blanche
                        $(obj).attr("favori", "false");
                        $(obj).attr("src", $("#urletoileblanche").val());
                        supprimerLigneDeMesFavoris($(obj));//On lui passe donc le td de la musique dans "récents"
                        //et supprimer la musique dans le tableau des favoris
                    }
                }
        ).fail(function (xhr) {
            alert(xhr.responseText);
        });
    }
}