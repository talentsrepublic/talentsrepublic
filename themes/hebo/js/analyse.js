/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


estLu = false;
estPause = false;

//on charge les différentes données à traiter
function loadStat()
{
    //si l'utilisateur change de musique sans stopper celle d'avant
    if(estLu)
    {
       stopStat();  
    }

    console.log("load");
   
    //c'est quand la musique commence à etre lu
    debut = new Date();
    
    //si la musique n'a pas encore été choisit ou a été stoppé
    estLu = true;
    
    //si la musique est en pause ou non
    estPause = false;
    
    //si le premier clic à été fait
    aClique = false;
    
    //tableau qui comptabilise le nombre de pose et la longeur de chacune
    nbPause = [];
    
    //tableau qui comptabilise le nombre de mouvement avec [i][0] = position avant clic et [i][1] = position apres clic
    nbMove = [];
    incrMove = 0;
   
    if(typeof id == "undefined")
        id = -1;
    
    //si la musique est réécoutée
    repeat = false;
    
    //pointeur établi
    getPointeurMusique();
    
    tpsAvantPremierClic = 0;
    
}

//bouton stop
function stopStat()
{
    if(estLu)
    {

        if(estPause)
        {
            playStat();
        }

        console.log("stop");
        
        estLu = false;
        fin = new Date();
        tempsMs = fin.getTime() - debut.getTime();
        
        //on soustrait le temps des pauses pour avoir l'écoute finale
        nbPause.forEach(function(element) {
            tempsMs = tempsMs - element;
        });
        
        tempsMs = tempsMs / 1000;

        //afficherStat();
        validerAjaxStat();
    }
}

//play est pause quand la musique a déja été lancé une fois
function playStat()
{
    console.log("choix pause/play");
    
    //si ce n'est pas la première lecture
    if(estLu)
    {
        if(estPause)
        {
            console.log("play");
            estPause = false;
            finStop = new Date();
            nbPause.push((finStop.getTime() - stop.getTime()));
        }
        else
        {
            if(aClique == false)
                getPremierClic();
            console.log("pause");
            stop = new Date();
            estPause = true;
        }
    }
    else
        loadStat();
}

//deplacement sur la barre de lecture
function moveStat(evt)
{
    if(aClique == false)
        getPremierClic();
    console.log("move");
    if(estLu)
    {
        getPointeurMusique();
        var avant = pointeurMusique;//position avant le clic
        var apres = getAfterClick(evt);
        nbMove[incrMove] = new Array();
        nbMove[incrMove].push(avant);
        nbMove[incrMove].push(apres);
        incrMove++;
        console.log(avant + " et " + apres);
    }
}

//convertir  charactere en seconde
function charToSec(char)
{   
    res = 0;
    sec = char.split(':');
    res = parseFloat(sec[0]) * 60; //minutes en secondes :    25:--
    res = res + parseFloat(sec[1]); // ajout des secondes restante  --:53
    return res;
}

//seconde en minute
function secToMin(sec)
{
    res = Math.trunc(sec / 60);
    res = res + (Math.trunc(sec % 60) / 100);
    return res;
}

//recupere position apres le clic
function getAfterClick(evt)
{
    if(estLu == false)
    {
        dureeMusique = charToSec($(".jp-duration").text().substr(1)) + pointeurMusique; //substr pour le "-"
    }
    var click = evt.clientX;
    var left = $(".jp-seek-bar").offset().left;
    var widthBar = $(".jp-seek-bar").width();
    var widthClick = click - left;
    var pourcent = (widthClick/widthBar)*100;
    getDureeMusique();
    var res = Math.round((dureeMusique*pourcent)/100);
    return res;
}


function getPointeurMusique()
{
    pointeurMusique = charToSec($(".jp-current-time").text());
}


function getDureeMusique()
{
    getPointeurMusique();
    //duree de la musique courante
    dureeMusique = charToSec($(".jp-duration").text().substr(1)) + pointeurMusique; //substr pour le "-"
    console.log(dureeMusique);
}

//affichage pour le test
function afficherStat()
{
    alert("Le script a mis " + tempsMs + " secondes sur " + secToMin(dureeMusique) + " minutes");
    alert("les pauses on été de : ");
    nbPause.forEach(function(element){alert(element/1000 + " secondes");});
    alert("les mouvement se sont fait de : ");
    nbMove.forEach(function(element){
        alert(element[0] + " secondes à " + element[1] + " secondes");
        if(element[0] > element[1])
            alert("il est revenu en arrière !");
    });
}



function getIdMusique(evt)
{
   if(id != $(evt).parents('.getId').attr('idmusique'))
   {
       id = $(evt).parents('.getId').attr('idmusique');
       repeat = false;
       console.log(id);
   }
   else
   {
       repeat = true;
       console.log("repeat");
   }
}




function validerAjaxStat()
{
    console.log("go");
     $.ajax({
       url : $('#adresseStat').val(), // La ressource ciblée
       type : 'GET', // Le type de la requête HTTP.
       data : 'idMusique=' + id + "&nbMove=" + JSON.stringify(nbMove) + "&nbPause=" + JSON.stringify(nbPause) + "&tempsEcoute=" + tempsMs + "&dureeMusc=" + dureeMusique + "&tpsAvantPremierClic=" + tpsAvantPremierClic + "&repeat=" + repeat,
       success:function(res){
            console.log(res);
       }
    });
    
    console.log("done");

}


function getPremierClic()
{
    aClique = true;
    var premierClic = new Date();
    tpsAvantPremierClic = (debut.getTime() - premierClic.getTime())/1000;
}


function testEstFini()
{
    if(estLu)
    {
        getDureeMusique();
        var dureeCour = charToSec($(".jp-current-time").text());
        if(dureeCour == dureeMusique && dureeMusique != 0)
        {
            stopStat();
        }
    }
}