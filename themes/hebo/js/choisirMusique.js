
function ajouteMusique(musiqueCour){
    if($("#musiqueStock").val().indexOf(musiqueCour + ";") == -1) //existe deja ?
    {
        $("#tableContient").append("<li style=\"list-style-type:none;\">"+musiqueCour+"</li>");
        var deleteButton = $("<div/>");
        deleteButton
                .css("display", "inline-block")
                .attr("id","deleteButton")
                .append("<img style=\"width:40px;height:40px;\" src=\""+$('#adresse').val()+"/img/trash.png"+"\"/>")
                .click(deleteMusique);
        $("#tableContient").children().last().append(deleteButton);
        $("#musiqueStock").val($("#musiqueStock").val()+musiqueCour+";"); 
    }
    else
            alert("cette musique a déjà été ajoutée ou n'existe pas");
}

function deleteMusique() { //supprimer dans le stockage (caché)

	var muscSuppr = $(this).parent().text() + ";";
        $('#musiqueStock').val($('#musiqueStock').val().replace(muscSuppr, ''));
	$(this).parent().remove();
}

function deleteRow(btn) { //supprime dans le tableau 
	
	var id = ($("tr").index($(btn).parent().parent()))-1;
	var muscSuppr = $(btn).parent().parent().children().eq(0).text();
	subMusc = indexbyIndex($('#musiqueStock').val(), id, ";");

	$('#musiqueStock').val($('#musiqueStock').val().substring(0,subMusc)+$('#musiqueStock').val().substring(muscSuppr.length+1+subMusc));
	$(btn).parent().parent().remove();
}

function indexbyIndex(string, indexOccurence, pattern){
	//indexbyIndex("beurre;farine;sel;pomme", 2, ";");
	var searchedIndex = 0;
	for(i=0 ; i<indexOccurence ; i++){
		searchedIndex = string.indexOf(pattern, searchedIndex+1);
	}
	return searchedIndex;
}

function resetData(elem)
{
	$(elem).val("");
}

function validerAjax(elem)
{
    $.ajax({
       url : $('#adresse').val()+"/index.php/musique/getMusic", // La ressource ciblée
       type : 'GET', // Le type de la requête HTTP.
       data : 'titre=' + elem,
       success : function(result){
           result = JSON.parse(result);
            if(result.length != 0)
                ajouteMusique(elem);
            else
                alert("La musique n'existe pas");
       },
       error:function(data){
           console.log(data);
       }
    });
}