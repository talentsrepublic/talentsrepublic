<?php

class AlbumController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'suggestMusique'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'suggestMusique'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'expression' => 'Yii::app()->user->isAdmin()',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->renderPartial('view', array(
            'model' => $this->loadModel($id),
        ),false,true);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Album;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        
        if (isset($_POST['Album']) && !Yii::app()->user->isGuest) {
            
            $codeErreur = true;
            $model->attributes = $_POST['Album'];
            $model->idutilisateur = Yii::app()->user->idutilisateur;
            if($codeErreur = $model->save()){
                $listMusique = explode(";", $_POST['musiqueStock']);
                for ($i = 0; $codeErreur && $i < count($listMusique) - 1; $i++) {
                    $musique = Musique::model()->findByAttributes(array('titre' => $listMusique[$i]));
                    $$i = new Appartient;
                    $$i->idalbum = $model->idalbum;
                    $$i->idmusique = $musique->idmusique;
                    $codeErreur = $codeErreur && ($$i->save());
                }                
            }
            if($codeErreur)
            {
                $this->renderPartial('view', array(
                    'model'=>Album::model()->with('musiques')->findByPk($model->idalbum)
                    ), false, true);
            } else {
                $this->renderPartial('create', array('model' => $model, 'message'=>"Erreur de création de l'album"), false, true);
            }
                //$this->renderPartial(array('../utilisateur/view', 'id' => Yii::app()->user->idutilisateur), false, true);//, 'codeErreur'=>true), false, true);
            
        } else {
            $this->renderPartial('create', array('model' => $model), false, true);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Album'])) {
            $model->attributes = $_POST['Album'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->idalbum));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Album');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Album('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Album']))
            $model->attributes = $_GET['Album'];

        $this->renderPartial('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Album the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Album::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Album $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'album-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionSuggestMusique() {
        if (!Yii::app()->user->isGuest) {
            $request = trim($_GET['term']);
            $id = Yii::app()->user->idutilisateur;
            $request = strtolower(addcslashes($request, '%_')); // escape LIKE's special characters

            $criteria = new CDbCriteria(array(
                'condition' => "lower(titre) LIKE :match AND idutilisateur=:id",
                'params' => array(':match' => "%$request%", ':id' => $id)
                    ));
            $model = Musique::model()->findAll($criteria);
            $data = array();
            foreach ($model as $musique) {
                $data[] = $musique->titre;
            }
            echo CJSON::encode($data);
            Yii::app()->end();
        }
    }

}