<?php

class WebUser extends CWebUser {
  // Store model to not repeat query.
  private $_model;
  
  // Return first name.
  // access it by Yii::app()->user->first_name
  function getIdUtilisateur(){
    $user = $this->loadUser(Yii::app()->user->id);
    return $user->idutilisateur;
  }
  
  function getPrenom(){
    $user = $this->loadUser(Yii::app()->user->id);
    return $user->prenom;
  }
 
  function getNom(){
    $user = $this->loadUser(Yii::app()->user->id);
    return $user->nom;
  }
  
  function getPseudo(){
    $user = $this->loadUser(Yii::app()->user->id);
    return $user->pseudo;
  }
  
  function getMail(){
    $user = $this->loadUser(Yii::app()->user->id);
    return $user->mail;
  }  
  
  function getVilleresidence(){
    $user = $this->loadUser(Yii::app()->user->id);
    return $user->villeresidence;
  }
  
  function getUrlPhoto(){
      $user = $this->loadUser(Yii::app()->user->id);
      return $user->urlphoto;
  }
  
  public function getInvitationsAmis(){
      $user = $this->loadUser(Yii::app()->user->id);
      return $user->invitationsamis;
  }
  
  public function getAmis(){
      $user = $this->loadUser(Yii::app()->user->id);
      return $user->amis;
  }
  
  public function getPosts($offset){
      $user = $this->loadUser(Yii::app()->user->id);
      return $user->getPosts($offset);
  }
  
  // This is a function that checks the field 'role'
  // in the User model to be equal to 1, that means it's admin
  // access it by Yii::app()->user->isAdmin()
  function isAdmin(){
    if(!Yii::app()->user->isGuest){  
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->typeutilisateur == "admin";
    }else
        return false;
  }
 
  // Load user model.
  protected function loadUser($mail=null)
    {
        if($this->_model===null)
        {
            if($mail!==null)
                $this->_model=Utilisateur::model()->findByAttributes(array('mail'=>$mail));
        }
        return $this->_model;
    }
}

