<?php

/**
 * This is the model class for table "utilisateur".
 *
 * The followings are the available columns in table 'utilisateur':
 * @property integer $idutilisateur
 * @property integer $typeutilisateur
 * @property string $prenom
 * @property string $nom
 * @property string $pseudo
 * @property string $mail
 * @property string $motdepasse
 * @property string $urlphoto
 * @property integer $active
 */
class Utilisateur extends CActiveRecord {

    public $image;
    public $type;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'utilisateur';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idvilleresidence, active', 'numerical', 'integerOnly' => true),
            array('typeutilisateur', 'length', 'max' => 10),
            array('prenom, nom, pseudo, mail, motdepasse, villeresidence', 'required', 'message' => 'Le champs {attribute} est obligatoire !'),
            array('prenom, nom, mail, urlphoto', 'length', 'max' => 50, 'message' => 'Votre {attribute} est trop long !'),
            array('pseudo', 'length', 'max' => 20, 'message' => 'Votre {attribute} est trop long !'),
            array('pseudo', 'unique', 'message' => 'Le pseudo est déjà utilisé !'),
            array('mail', 'unique', 'message' => 'L\'adresse mail est déjà utilisée !'),
            array('mail', 'email', 'message' => 'L\'adresse mail est incorrecte !'),
            array('motdepasse', 'length', 'max' => 70, 'message' => 'Votre {attribute} est trop long !'),
            array('villeresidence', 'length', 'max' => 100, 'message' => 'Votre {attribute} est trop long !'),
            array('image', 'EImageValidator', 'min_size' => 0, 'max_size' => 6000, 'allowEmpty' => true, 'sizeError' => 'La taille de l\'image est trop grande (>8M) !'),
            array('image', 'EImageValidator', 'types' => "gif, jpg, png, GIF, JPG, PNG", 'allowEmpty' => true, 'typesError' => 'Le type d\'image doit être jpg, png ou gif !'),
            array('image', 'file', 'types' => 'jpg, gif, png, JPG, GIF, PNG', 'allowEmpty' => true, 'on' => 'insert,update', 'message' => 'Le type de fichier doit être : jpg, gif ou png!'), // this will allow empty field when page is update (remember here i create scenario update)
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('idutilisateur, typeutilisateur, prenom, nom, pseudo, mail, motdepasse, urlphoto, villeresidence, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'amis' => array(self::MANY_MANY, 'Utilisateur', 'estamis(idutilisateur,idutilisateur2)', 'condition' => 'demandeenattente = 0'),
            'invitationsamis' => array(self::MANY_MANY, 'Utilisateur', 'estamis(idutilisateur2,idutilisateur)', 'condition' => 'demandeenattente = 1'),
            'villeresidence' => array(self::BELONGS_TO, 'Villeresidence', 'idvilleresidence'),
            'mesposts' => array(self::HAS_MANY, 'Post', 'idutilisateur')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'idutilisateur' => 'Idutilisateur',
            'typeutilisateur' => 'Type utilisateur',
            'prenom' => 'Prenom',
            'nom' => 'Nom',
            'pseudo' => 'Pseudo',
            'mail' => 'Mail',
            'motdepasse' => 'Mot de passe',
            'urlphoto' => 'Urlphoto',
            'villeresidence' => 'Ville de résidence'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idutilisateur', $this->idutilisateur);
        $criteria->compare('typeutilisateur', $this->typeutilisateur, true);
        $criteria->compare('prenom', $this->prenom, true);
        $criteria->compare('nom', $this->nom, true);
        $criteria->compare('pseudo', $this->pseudo, true);
        $criteria->compare('mail', $this->mail, true);
        $criteria->compare('motdepasse', $this->motdepasse, true);
        $criteria->compare('urlphoto', $this->urlphoto, true);
        $criteria->compare('idvilleresidence', $this->idvilleresidence, true);
        $criteria->compare('active', $this->active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Utilisateur the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getPosts($offset) {
        $result = array();
        if (!Yii::app()->user->isGuest) {
            //mes posts + les posts des amis
            $sql = "SELECT p0.* FROM post p0 
                    left join commentaire c on c.idpost=p0.idpost 
                    WHERE p0.idutilisateur=:id OR p0.idutilisateur IN ( 
                    select e.idutilisateur2 from utilisateur u2 
                    JOIN estamis e on u2.idutilisateur = e.idutilisateur and u2.idutilisateur=:id and demandeenattente=0) 
                    GROUP BY p0.idpost 
                    ORDER BY date DESC 
                    LIMIT ".Post::LIMIT." OFFSET $offset";
            
            $result = Post::model()->with('commentaires')->findAllBySql($sql, array(':id'=>Yii::app()->user->idutilisateur));
            
        }
        return $result;
    }

    public function invalider() {
        $this->active = 0;
        return $this->update();
    }

    public function valider() {
        $this->active = 1;
        return $this->update();
    }

}
