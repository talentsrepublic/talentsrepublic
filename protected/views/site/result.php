

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/analyse.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/lecturemusique.js"></script>


<?php





if (!isset($musiquestrouvees) || !isset($artistestrouves)) {
    ?>
    <h1>Aucun résultat trouvé !</h1>
<?php } else {
    ?>


    <h1>Resultat de la recherche</h1>


    <ul class="nav nav-tabs">
        <li class="active"><a href="#musiques" data-toggle="tab">Musiques</a></li>
        <li><a href="#artistes" data-toggle="tab">Artistes</a></li>    
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="musiques">
            <table class="table table-striped table-hover">
                <tbody>
                    <?php
                    if (count($musiquestrouvees) == 0)
                        echo "<tr><td>Aucune musique n'a été trouvée !</td></tr>";
                    else {
                        foreach ($musiquestrouvees as $musique) {
                            ?>
                            <tr class="getId" idmusique="<?php echo $musique->idmusique; ?>"><td>
                                    <div>
                                        <div class="jp-controls musique" titre="<?php echo $musique->titre; ?>" url="<?php echo Yii::app()->request->hostinfo . '/' . Yii::app()->request->baseUrl . '/musique/' . $musique->urlmusique; ?>">
                                            <button class="jp-play" onclick="getDureeMusique();loadMusique(this);loadStat();getIdMusique(this);" role="button" tabindex="0">play</button>
                                            <button class="jp-stop" onclick="stop();stopStat();"role="button" tabindex="0">stop</button>
                                        </div>
                                        <?php
                                        echo $musique->titre . ' de ';
                                        if ($musique->artiste != null) {
                                            echo CHtml::ajaxLink(
                                                    $musique->artiste->pseudo, array('utilisateur/' . $musique->artiste->idutilisateur), array(
                                                'type' => 'POST',
                                                'success' => 'function(html){ $("#content").empty(); $("#content").append(html); }',
                                                'error' => 'function(html){console.log(html);}'
                                                    )
                                            );
                                        }
                                        ?>
                                    </div>
                                </td></tr>
                            <?php
                        }
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="tab-pane" id="artistes">   
        <table class="table table-striped table-hover">
            <tbody>
                <?php
                if (count($artistestrouves) == 0)
                    echo "<tr><td>Aucun artiste n'a été trouvé !</td></tr>";
                else {
                    foreach ($artistestrouves as $artiste) {
                        ?>
                        <tr><td>

                                <?php
                                echo CHtml::ajaxLink(
                                        $artiste->pseudo, array('utilisateur/' . $artiste->idutilisateur), array(
                                    'type' => 'POST',
                                    'success' => 'function(html){ $("#content").empty(); $("#content").append(html); }',
                                    'error' => 'function(html){console.log(html);}'
                                        )
                                );
                                ?>
                            </td></tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>