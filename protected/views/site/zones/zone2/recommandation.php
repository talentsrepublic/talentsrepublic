<?php
if(count($recommandations)>0){?>
    
<input type="hidden" id="urlcomplete" value="<?php echo Yii::app()->request->hostinfo.Yii::app()->request->baseUrl; ?>"/>
<input type="hidden" id="urletoilejaune" value="<?php echo Yii::app()->theme->baseUrl . '/img/etoile-jaune.png'; ?>"/>
<input type="hidden" id="urletoileblanche" value="<?php echo Yii::app()->theme->baseUrl . '/img/etoile-blanche.png'; ?>"/>
<?php if (!Yii::app()->user->isGuest){ ?><input type="hidden" id="idutilisateur" value="<?php echo Yii::app()->user->idutilisateur; ?>"/><?php } ?>
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>Titre</th>
            <th>Artiste</th>
            <th>Album(s)</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($recommandations as $musique) {
            echo "<tr>\n";
            echo '<td class="getId" idmusique="'.$musique->idmusique.'">';
            ?><div class="jp-controls musique" titre="<?php echo $musique->titre; ?>" url="<?php echo Yii::app()->request->hostinfo . '/' . Yii::app()->request->baseUrl . '/musique/' . $musique->urlmusique; ?>">
            <button class="jp-play" onclick="getDureeMusique();loadMusique(this);loadStat();getIdMusique(this);" role="button" tabindex="0">play</button>
            <button class="jp-stop" onclick="stop();stopStat();"role="button" tabindex="0">stop</button>
        </div>
        <?php
        if (!Yii::app()->user->isGuest) {

            if ($musique->estDansMesFavoris(Yii::app()->user->idutilisateur))
                echo '<img class="etoile" onClick="gererFavori(this)" src="' . Yii::app()->theme->baseUrl . '/img/etoile-jaune.png" favori="true" idmusique="'.$musique->idmusique.'" />';
            else
                echo '<img class="etoile" onClick="gererFavori(this)" src="' . Yii::app()->theme->baseUrl . '/img/etoile-blanche.png" favori="false" idmusique="'.$musique->idmusique.'"/>';
        }

        echo $musique->titre . '</td>' . "\n";
        echo '<td>';
        if ($musique->artiste != null)
        {
            echo CHtml::ajaxLink(
                $musique->artiste->pseudo, 
                array('utilisateur/'.$musique->artiste->idutilisateur), 
                array (
                    'type'=>'POST',
                    'success'=>'function(html){ $("#content").empty(); $("#content").append(html); }',
                    'error'=>'function(html){console.log(html);}'
                    )
                );
        }else
            echo 'Artiste non renseigné !';
        echo '</td>' . "\n";
        echo '<td>';
        $nbalbums = count($musique->albums);
        if($nbalbums==0){
            echo 'Aucun album !';
        }else{
            $i=0;
            while($i<$nbalbums-1){
                echo $musique->albums[$i]->libellealbum.', ';
                $i++;
            }
            echo $musique->albums[$i]->libellealbum;
        }
        echo '</td>';
        echo "</tr>\n";
    }
    ?>
</tbody>
</table>
<?php }else
    echo '<h3>Aucune musique à afficher</h3>';