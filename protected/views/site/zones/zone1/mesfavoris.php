<div class="tab-pane" id="mesfavoris">

    <?php
    if (count($musiquesfavoris) == 0) {
        ?><h3>Vous n'avez pas de musiques dans vos favoris !</h3>
        <table class="table table-striped table-bordered table-hover">
            <tbody></tbody>
        </table>
    <?php
    } else {
        echo "<h3>Vos musiques favorites :</h3>";
        ?><table class="table table-striped table-bordered table-hover">
            <tbody>
    <?php foreach ($musiquesfavoris as $musique) { ?>
                    <tr idmusique="<?php echo $musique->idmusique; ?>"><td>
                            <div>
                                <div class="jp-controls musique" titre="<?php echo $musique->titre; ?>" url="<?php echo Yii::app()->request->hostinfo . '/' . Yii::app()->request->baseUrl . '/musique/' . $musique->urlmusique; ?>">
                                    <button class="jp-play" onclick="loadMusique(this)" role="button" tabindex="0">play</button>
                                    <button class="jp-stop" onclick="stop()"role="button" tabindex="0">stop</button>
                                </div>
                                <?php
                                echo $musique->titre . ' de ';
                                if ($musique->artiste != null) {
                                    echo CHtml::ajaxLink(
                                            $musique->artiste->pseudo, array('utilisateur/' . $musique->artiste->idutilisateur), array(
                                        'type' => 'POST',
                                        'success' => 'function(html){ $("#content").empty(); $("#content").append(html); }',
                                        'error' => 'function(html){console.log(html);}'
                                            )
                                    );
                                }
                                ?>
                            </div>
                        </td></tr>
                <?php }
                ?>
            </tbody>
        </table>
        <?php
    }
    ?>
</div>
