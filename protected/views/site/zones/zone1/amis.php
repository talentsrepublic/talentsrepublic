<div class="tab-pane" id="amis">
    <?php
    
    //Affichage des invitations
    //print_r(Yii::app()->user->getInvitationsAmis());
    if (count(Yii::app()->user->getInvitationsAmis())>0) {
        echo '<h3 id="invittitre">Nouvelles invitations</h3>';
        foreach (Yii::app()->user->getInvitationsAmis() as $invitation) {
            echo '<div>';

            echo CHtml::image(Yii::app()->baseUrl . '/img/photosprofiles/' . $invitation->urlphoto, Yii::app()->baseUrl . '/img/photosprofiles/team-member.png', array('width' => '80px', 'height' => '80px'));
            echo $invitation->prenom . ' ' . $invitation->nom;
            echo '<div class="invitation" style="float:right;height:40px; padding-top:20px;">';
            echo CHtml::ajaxSubmitButton('Accepter l\'invitation', array('utilisateur/accepterami'), array('data' => array('idami' => $invitation->idutilisateur),
                'success' => 'function(data){   
                                                var res = JSON.parse(data);
                                                alert(res.message);
                                                if(res.status){
                                                    var div = $("#btn_accepterami' . $invitation->idutilisateur . '").parent().parent();
                                                    $("#btn_accepterami' . $invitation->idutilisateur . '").parent().parent().remove();
                                                    div.children().last().remove();
                                                    $("#amish3").after(div.html());
                                                    if($(".invitation").length==0)
                                                    {
                                                        $("#invittitre").text("Aucune invitation");
                                                    }
                                                }
                                            }'
                    ), array('id' => 'btn_accepterami' . $invitation->idutilisateur, 'class' => 'btn-success'));
            echo ' ';
            echo CHtml::ajaxSubmitButton('Refuser l\'invitation', array('utilisateur/refuserami'), array('data' => array('idami' => $invitation->idutilisateur),
                'success' => 'function(data){                                   
                                                var res = JSON.parse(data);
                                                alert(res.message);
                                                if(res.status){
                                                    $("#btn_accepterami' . $invitation->idutilisateur . '").parent().parent().remove();
                                                    if($(".invitation").length==0)
                                                    {
                                                        $("#invittitre").text("Aucune invitation");
                                                        $("a#amis").text("Amis");
                                                    }
                                                }

                                            }'
                    ), array('id' => 'btn_refuserami' . $invitation->idutilisateur, 'class' => 'btn-danger'));
            echo '</div>';
            echo '</div>';
        }
    } else
        echo '<h3>Aucune invitation</h3>';

    //Affichage des amis
    $nbAmis = count(Yii::app()->user->getAmis());
    if ($nbAmis > 0) {
        echo '<h3 id="amish3">Mes amis</h3>';
        foreach (Yii::app()->user->getAmis() as $ami) {
            echo '<div>';
            echo CHtml::image(Yii::app()->baseUrl . '/img/photosprofiles/' . $ami->urlphoto, Yii::app()->baseUrl . '/img/photosprofiles/team-member.png', array('width' => '80px', 'height' => '80px'));
            echo $ami->prenom . ' ' . $ami->nom;
            
            //Bouton supprimer en amis
            //Si l'invitation est envoyée (que l'autre ait répondu ou non)
            echo CHtml::ajaxSubmitButton('Retirer de ma liste d\'amis', array('utilisateur/supprimerami'), array('data' => array('idami' => $ami->idutilisateur),
                'success' => 'function(data){                                             
                                                var res = JSON.parse(data);
                                                alert(res.message);
                                                if(res.status)
                                                {
                                                    $("#btn_supprimerami'.$ami->idutilisateur.'").parent().remove();
                                                }

                                            }'
                    ), array('id' => 'btn_supprimerami'.$ami->idutilisateur, 'class' => 'btn btn-danger', 'style'=>'float:right;margin-top:30px;'));

            echo '</div>';
        }
    } else {
        echo '<h3>La liste d\'amis est vide !</h3>';
    }
    ?>

</div>
