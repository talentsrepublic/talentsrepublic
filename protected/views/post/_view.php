<?php
/* @var $this PostController */
/* @var $data Post */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idpost')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idpost), array('view', 'id'=>$data->idpost)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idutilisateur')); ?>:</b>
	<?php echo CHtml::encode($data->idutilisateur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contenu')); ?>:</b>
	<?php echo CHtml::encode($data->contenu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />


</div>