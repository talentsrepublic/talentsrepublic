<?php
/* @var $this PostController */
/* @var $dataProvider CActiveDataProvider */
if (!empty($posts)) {

    foreach ($posts as $post) {
        echo '<tr idpost="' . $post->idpost . '">';
        echo '<td style="vertical-align:middle;">';
        echo "<div>";
        echo '<img style="width:40px;height:50px;" src="' . Yii::app()->baseUrl . '/img/photosprofiles/' . $post->utilisateur->urlphoto . '"/>';
        echo "\t" . $post->utilisateur->pseudo;
        if (Yii::app()->user->idutilisateur == $post->idutilisateur) {
            ?>
                <input class="btn btn-danger btn-small" style="float:right;" id="btnSupprimerPost<?php echo $post->idpost; ?>" type="button" onclick='supprimerPost("<?php echo Yii::app()->createUrl('post/delete', array('id'=>$post->idpost));?>",<?php echo $post->idpost; ?>)' value="Supprimer"/>
            <?php
            /*echo CHtml::ajaxSubmitButton(
                'Supprimer', Yii::app()->createUrl('post/delete', array("id" => $post->idpost)), array(
                'type' => 'POST',
                'success' => 'function(){ $("tr[idpost=' . $post->idpost . ']").remove();}',
                'error' => 'function(result){console.log(result);}'
                    ), array('style' => 'float:right;', 'class' => 'btn btn-danger btn-small'));*/
        }
        echo "</div>";


        echo "<p>" . $post->contenu . "</p>";
        echo "<div style=\"float:right;font-size:10px;\">Publié le " . $post->date . "</div>";
        echo '<div style="clear:both;"></div>';
        ?><textarea id="commentaire<?php echo $post->idpost; ?>" placeholder="Posez votre commentaire ici !" style="width:90%;"></textarea>
        <input type="hidden" id="valeurcommentaire<?php echo $post->idpost;?>" value=""/>
        <script>
            $("#commentaire<?php echo $post->idpost; ?>").bind("input propertychange", function(){
               $("#valeurcommentaire<?php echo $post->idpost;?>").val($("#commentaire<?php echo $post->idpost; ?>").val()); 
            });
        </script>
        
        <input style="float:right;" type="submit" onclick="commenter(this, <?php echo $post->idpost; ?>)" id="btnCommentaire<?php echo $post->idpost; ?>" value="Commenter" class="btn btn-primary btn-small"/>
        <div style="clear:both;"></div>
        <?php
         
        //Affichage des commentaires
        if (!empty($post->commentaires)) {
            echo "<a id=\"btnVoirCommentaires" . $post->idpost . "\" onClick=\"voirTousLesCommentaires(" . $post->idpost . ");\">Voir les commentaires</a>";
            echo "<a id=\"btnCacherCommentaires" . $post->idpost . "\" class=\"btnCacherCommentaires\" onClick=\"cacherTousLesCommentaires(" . $post->idpost . ");\">Cacher les commentaires</a>";
            echo "<table style=\"width:100%;\"class=\"commentaires\" id=\"commentairesDuPost" . $post->idpost . "\">";
            echo "<tbody>";

            foreach ($post->commentaires as $commentaire) {

                echo '<tr idcommentaire="' . $commentaire->idcommentaire . '">';

                echo "<td style=\"vertical-align:middle;\">";
                echo "<div>";
                echo "<img style=\"width:40px;height:50px;\" src=\"" . Yii::app()->baseUrl . '/img/photosprofiles/' . $commentaire->utilisateur->urlphoto . "\">";
                echo "\t" . $commentaire->utilisateur->pseudo;
                //On peut supprimer son commentaire mais aussi le commentaire d'un autre s'il est relatif au post courant
                if (Yii::app()->user->idutilisateur == $commentaire->idutilisateur || Yii::app()->user->idutilisateur == $post->idutilisateur) {

                    echo CHtml::ajaxSubmitButton(
                        'Supprimer', Yii::app()->createUrl('commentaire/delete', array("id" => $commentaire->idcommentaire)), array(
                        'type' => 'POST',
                        'success' => 'function(){ $("tr[idcommentaire=' . $commentaire->idcommentaire . ']").remove();}',
                        'error' => 'function(result){console.log(result);}'
                            ), array('style' => 'float:right;', 'class' => 'btn btn-danger btn-small'));
                }

                echo "</div>";
                echo "<p>" . $commentaire->contenu . "</p>";
                echo "<div style=\"float:right;font-size:10px;\">Publié le " . $commentaire->date . "</div>";
                echo "</td></tr>";
            }

         
         
            echo "</tbody>";
            echo "</table>";
        }

        echo "</td>";
        echo "</tr>";
    }
}
?>