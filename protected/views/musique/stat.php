<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$data1 = $dataProvider1->getData(); //sept derniers jour : ecoutes positives
$data2 = $dataProvider2->getData(); //sept derniers jour : ecoutes negatives

$data3 = forWeek($dataProvider3->getData()); //jour de la seamine : ecoutes positives
$data4 = forWeek($dataProvider4->getData()); //jour de la seamine : ecoutes negatives

$data5 = $dataProvider5; //par semaine : ecoutes positives
$data6 = $dataProvider6; //par semaine : ecoutes negatives

$data7 = $dataProvider7; //par mois : ecoutes positives
$data8 = $dataProvider8; //par mois : ecoutes negatives

$data9 = $dataProvider9; //sept derniers jour : ecoutes negatives
$data10 = forWeek($dataProvider10->getData()); //jour de la seamine : ecoutes positives
$data11 = $dataProvider11; //par semaine : ecoutes negatives
$data12 = $dataProvider12; //par mois : ecoutes positives

?>

<script src="http://code.highcharts.com/highcharts.js"></script>



<select id="selectBox" onchange="afficherChart()">
    <option value="cool" >Ecoutes positives</option>
    <option value="pascool" >Ecoutes negatives</option>
    <option value="tous" >Toutes les ecoutes</option>
    <option value="autre" >Autres informations</option>
</select>



<script>
function afficherChart()
{
    var selectBox = document.getElementById("selectBox");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    if(selectedValue == "cool")
    {
        $('.pascool, .tous, .autre').css('display','none'); $('.cool').css('display','initial');
    }
    if(selectedValue == "pascool")
    {
        $('.cool, .tous, .autre').css('display','none'); $('.pascool').css('display','initial');
    }
    if(selectedValue == "tous")
    {
        $('.pascool, .cool, .autre').css('display','none'); $('.tous').css('display','initial');
    }
    if(selectedValue == "autre")
    {
        $('.pascool, .cool, .tous').css('display','none'); $('.autre').css('display','initial');
    }
}
</script>

<div>
<div class="cool" id="septEcouteCool" style="width:45%; height:350px; margin-right:2.5%; float:left;"></div>
<div class="cool" id="semaineEcouteCool" style="width:45%; height:350px; margin-left:2.5%;float:left;"></div>
<div class="cool" id="moisEcouteCool" style="width:45%; height:350px; margin-right:2.5%;margin-top:2.5%;float:left;"></div>
<div class="cool" id="anneeEcouteCool" style="width:45%; height:350px; margin-left:2.5%;margin-top:2.5%;float:left;"></div>

<div class="pascool" id="septEcoutePasCool" style="width:45%; height:350px; margin-right:2.5%; float:left;"></div>
<div class="pascool" id="semaineEcoutePasCool" style="width:45%; height:350px; margin-left:2.5%;float:left;"></div>
<div class="pascool" id="moisEcoutePasCool" style="width:45%; height:350px; margin-right:2.5%;margin-top:2.5%;float:left;"></div>
<div class="pascool" id="anneeEcoutePasCool" style="width:45%; height:350px; margin-left:2.5%;margin-top:2.5%;float:left;"></div>

<div class="tous" id="septEcouteTous" style="width:45%; height:350px; margin-right:2.5%; float:left;"></div>
<div class="tous" id="semaineEcouteTous" style="width:45%; height:350px; margin-left:2.5%;float:left;"></div>
<div class="tous" id="moisEcouteTous" style="width:45%; height:350px; margin-right:2.5%;margin-top:2.5%;float:left;"></div>
<div class="tous" id="anneeEcouteTous" style="width:45%; height:350px; margin-left:2.5%;margin-top:2.5%;float:left;"></div>

<div class="autre" id="pourcentEcoute" style="width:45%; height:350px; margin-right:2.5%;float:left;"></div>
<div class="autre" id="nbEcoute" style="width:45%; height:350px; margin-left:2.5%;float:left;"></div>
</div>



<script>
Highcharts.chart('septEcouteCool', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoutes positives les 7 derniers jours (où la musique a au moins été écoutée une fois)"
    },
    xAxis: {
        "type": "datetime",
        "labels": {
            "format": "{value:%Y %b %d}"
        },
        categories: [<?php echo (isset($data1[0]) ? strtotime($data1[0]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data1[1]) ? strtotime($data1[1]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data1[2]) ? strtotime($data1[2]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data1[3]) ? strtotime($data1[3]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data1[4]) ? strtotime($data1[4]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data1[5]) ? strtotime($data1[5]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data1[6]) ? strtotime($data1[6]->dateecoute)*1000+86400000 : "") ?>]
    },
    yAxis: {
        title: {
            text: "Nombre d'écoutes"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',                
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo (isset($data1[0]) ? $data1[0]->nb : "0")?>,
            drilldown: <?php echo (isset($data1[0]) ? $data1[0]->dateecoute : "0")?>
        }, {
            y:  <?php echo (isset($data1[1]) ? $data1[1]->nb : "0")?>,
            drilldown: <?php echo (isset($data1[0]) ? $data1[0]->dateecoute : "0")?>
        }, {
            y:  <?php echo (isset($data1[2]) ? $data1[2]->nb : "0")?>,
            drilldown: <?php echo (isset($data1[0]) ? $data1[0]->dateecoute : "0")?>
        }, {
            y: <?php echo (isset($data1[3]) ? $data1[3]->nb : "0")?>,
            drilldown: <?php echo (isset($data1[0]) ? $data1[0]->dateecoute : "0")?>
        }, {
            y:  <?php echo (isset($data1[4]) ? $data1[4]->nb : "0")?>,
            drilldown: <?php echo (isset($data1[0]) ? $data1[0]->dateecoute : "0")?>
        }, {
            y:  <?php echo (isset($data1[5]) ? $data1[5]->nb : "0")?>,
            drilldown: <?php echo (isset($data1[0]) ? $data1[0]->dateecoute : "0")?>
        }, {
            y:  <?php echo (isset($data1[6]) ? $data1[6]->nb : "0")?>,
            drilldown: null
        }]
    }]
});
</script>


<script>
Highcharts.chart('septEcoutePasCool', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoutes négatives les 7 derniers jours (où la musique a au moins été écoutée une fois)"
    },
    xAxis: {
        "type": "datetime",
        "labels": {
            "format": "{value:%Y %b %d}"
        },
        categories: [<?php echo (isset($data2[0]) ? strtotime($data2[0]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data2[1]) ? strtotime($data2[1]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data2[2]) ? strtotime($data2[2]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data2[3]) ? strtotime($data2[3]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data2[4]) ? strtotime($data2[4]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data2[5]) ? strtotime($data2[5]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data2[6]) ? strtotime($data2[6]->dateecoute)*1000+86400000 : "") ?>]
    },
    yAxis: {
        title: {
            text: "Nombre d'écoutes"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo (isset($data2[0]) ? $data2[0]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data2[1]) ? $data2[1]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data2[2]) ? $data2[2]->nb : "0")?>
        }, {
            y: <?php echo (isset($data2[3]) ? $data2[3]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data2[4]) ? $data2[4]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data2[5]) ? $data2[5]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data2[6]) ? $data2[6]->nb : "0")?>,
            drilldown: null
        }]
    }]
});
</script>

<script>
Highcharts.chart('semaineEcouteCool', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoutes positive cette semaine"
    },
    xAxis: {
        type: 'datetime',
        labels: {
            format: '{value:%Y-%b-%d}'
        }
    },
    yAxis: {
        title: {
            text: "Nombre d'écoutes"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',
                
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo $data3[0]?>
        }, {
            y:  <?php echo $data3[1]?>
        }, {
            y:  <?php echo $data3[2] ?>
        }, {
            y: <?php echo $data3[3] ?>
        }, {
            y:  <?php echo $data3[4] ?>
        }, {
            y:  <?php echo $data3[5] ?>
        }, {
            y:  <?php echo $data3[6] ?>
        }],
        pointStart: <?php echo strtotime('monday')*1000 ?>,
        pointInterval: 24 * 36e5
    }]
});
</script>

<script>
Highcharts.chart('semaineEcoutePasCool', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoutes négatives cette semaine"
    },
    xAxis: {
        type: 'datetime',
        labels: {
            format: '{value:%Y-%b-%d}'
        }
    },
    yAxis: {
        title: {
            text: "Nombre d'écoutes"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',                
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo $data4[0]?>
        }, {
            y:  <?php echo $data4[1]?>
        }, {
            y:  <?php echo $data4[2] ?>
        }, {
            y: <?php echo $data4[3] ?>
        }, {
            y:  <?php echo $data4[4] ?>
        }, {
            y:  <?php echo $data4[5] ?>
        }, {
            y:  <?php echo $data4[6] ?>
        }],
        pointStart: <?php echo strtotime('monday')*1000 ?>,
        pointInterval: 24 * 36e5
    }]
});
</script>

<script>
Highcharts.chart('moisEcouteCool', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoutes positives par semaine"
    },
    xAxis: {
        categories: [
            'semaine <?php echo (isset($data5[0]) ? $data5[0]->dateecoute : "")?>',
            'semaine <?php echo (isset($data5[1]) ? $data5[1]->dateecoute : "") ?>',
            'semaine <?php echo (isset($data5[2]) ? $data5[2]->dateecoute : "") ?>',
            'semaine <?php echo (isset($data5[3]) ? $data5[3]->dateecoute : "") ?>'
            ]
    },
    yAxis: {
        title: {
            text: "Nombre d'écoutes"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',                
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo (isset($data5[0]) ? $data5[0]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data5[1]) ? $data5[1]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data5[2]) ? $data5[2]->nb : "0")?>
        }, {
            y: <?php echo (isset($data5[3]) ? $data5[3]->nb : "0")?>
        }]
    }]
});
</script>


<script>
Highcharts.chart('moisEcoutePasCool', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoutes négatives par semaine"
    },
    xAxis: {
        categories: [
            'semaine <?php echo (isset($data6[0]) ? $data6[0]->dateecoute : "")?>',
            'semaine <?php echo (isset($data6[1]) ? $data6[1]->dateecoute : "") ?>',
            'semaine <?php echo (isset($data6[2]) ? $data6[2]->dateecoute : "") ?>',
            'semaine <?php echo (isset($data6[3]) ? $data6[3]->dateecoute : "") ?>'
            ]
    },
    yAxis: {
        title: {
            text: "Nombre d'écoutes"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo (isset($data6[0]) ? $data6[0]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data6[1]) ? $data6[1]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data6[2]) ? $data6[2]->nb : "0")?>
        }, {
            y: <?php echo (isset($data6[3]) ? $data6[3]->nb : "0")?>
        }]
    }]
});
</script>


<script>
Highcharts.chart('anneeEcouteCool', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoutes positives par mois"
    },
    xAxis: {
        categories: [
            '<?php echo (isset($data7[0]) ? $data7[0]->dateecoute : "")?>',
            '<?php echo (isset($data7[1]) ? $data7[1]->dateecoute : "") ?>',
            '<?php echo (isset($data7[2]) ? $data7[2]->dateecoute : "") ?>',
            '<?php echo (isset($data7[3]) ? $data7[3]->dateecoute : "") ?>'
            ]
    },
    yAxis: {
        title: {
            text: "Nombre d'écoutes"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo (isset($data7[0]) ? $data7[0]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data7[1]) ? $data7[1]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data7[2]) ? $data7[2]->nb : "0")?>
        }, {
            y: <?php echo (isset($data7[3]) ? $data7[3]->nb : "0")?>
        }]
    }]
});
</script>


<script>
Highcharts.chart('anneeEcoutePasCool', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoutes négatives par mois"
    },
    xAxis: {
        categories: [
            '<?php echo (isset($data8[0]) ? $data8[0]->dateecoute : "")?>',
            '<?php echo (isset($data8[1]) ? $data8[1]->dateecoute : "") ?>',
            '<?php echo (isset($data8[2]) ? $data8[2]->dateecoute : "") ?>',
            '<?php echo (isset($data8[3]) ? $data8[3]->dateecoute : "") ?>'
            ]
    },
    yAxis: {
        title: {
            text: "Nombre d'écoutes"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo (isset($data8[0]) ? $data8[0]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data8[1]) ? $data8[1]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data8[2]) ? $data8[2]->nb : "0")?>
        }, {
            y: <?php echo (isset($data8[3]) ? $data8[3]->nb : "0")?>
        }]
    }]
});
</script>


<script>
Highcharts.chart('septEcouteTous', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoutes les 7 derniers jours (où la musique a au moins été écoutée une fois)"
    },
    xAxis: {
        "type": "datetime",
        "labels": {
            "format": "{value:%Y %b %d}"
        },
        categories: [<?php echo (isset($data9[0]) ? strtotime($data9[0]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data9[1]) ? strtotime($data9[1]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data9[2]) ? strtotime($data9[2]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data9[3]) ? strtotime($data9[3]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data9[4]) ? strtotime($data9[4]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data9[5]) ? strtotime($data9[5]->dateecoute)*1000+86400000 : "") ?>,
            <?php echo (isset($data9[6]) ? strtotime($data9[6]->dateecoute)*1000+86400000 : "") ?>]
    },
    yAxis: {
        title: {
            text: "Nombre d'écoutes"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo (isset($data9[0]) ? $data9[0]->nb : "0")?>,
            drilldown: <?php echo (isset($data9[0]) ? $data9[0]->dateecoute : "0")?>
        }, {
            y:  <?php echo (isset($data9[1]) ? $data9[1]->nb : "0")?>,
            drilldown: <?php echo (isset($data9[0]) ? $data9[0]->dateecoute : "0")?>
        }, {
            y:  <?php echo (isset($data9[2]) ? $data9[2]->nb : "0")?>,
            drilldown: <?php echo (isset($data9[0]) ? $data9[0]->dateecoute : "0")?>
        }, {
            y: <?php echo (isset($data9[3]) ? $data9[3]->nb : "0")?>,
            drilldown: <?php echo (isset($data9[0]) ? $data9[0]->dateecoute : "0")?>
        }, {
            y:  <?php echo (isset($data9[4]) ? $data9[4]->nb : "0")?>,
            drilldown: <?php echo (isset($data9[0]) ? $data9[0]->dateecoute : "0")?>
        }, {
            y:  <?php echo (isset($data9[5]) ? $data9[5]->nb : "0")?>,
            drilldown: <?php echo (isset($data9[0]) ? $data9[0]->dateecoute : "0")?>
        }, {
            y:  <?php echo (isset($data9[6]) ? $data9[6]->nb : "0")?>,
            drilldown: null
        }]
    }]
});
</script>


<script>
Highcharts.chart('semaineEcouteTous', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoutes cette semaine"
    },
    xAxis: {
        type: 'datetime',
        labels: {
            format: '{value:%Y-%b-%d}'
        }
    },
    yAxis: {
        title: {
            text: "Nombre d'écoutes"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo $data10[0]?>
        }, {
            y:  <?php echo $data10[1]?>
        }, {
            y:  <?php echo $data10[2] ?>
        }, {
            y: <?php echo $data10[3] ?>
        }, {
            y:  <?php echo $data10[4] ?>
        }, {
            y:  <?php echo $data10[5] ?>
        }, {
            y:  <?php echo $data10[6] ?>
        }],
        pointStart: <?php echo strtotime('monday')*1000 ?>,
        pointInterval: 24 * 36e5
    }]
});
</script>


<script>
Highcharts.chart('moisEcouteTous', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoutes par semaine"
    },
    xAxis: {
        categories: [
            'semaine <?php echo (isset($data11[0]) ? $data11[0]->dateecoute : "")?>',
            'semaine <?php echo (isset($data11[1]) ? $data11[1]->dateecoute : "") ?>',
            'semaine <?php echo (isset($data11[2]) ? $data11[2]->dateecoute : "") ?>',
            'semaine <?php echo (isset($data11[3]) ? $data11[3]->dateecoute : "") ?>'
            ]
    },
    yAxis: {
        title: {
            text: "Nombre d'écoutes"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo (isset($data11[0]) ? $data11[0]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data11[1]) ? $data11[1]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data11[2]) ? $data11[2]->nb : "0")?>
        }, {
            y: <?php echo (isset($data11[3]) ? $data11[3]->nb : "0")?>
        }]
    }]
});
</script>

<script>
Highcharts.chart('anneeEcouteTous', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },    
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoutes par mois"
    },
    xAxis: {
        categories: [
            '<?php echo (isset($data12[0]) ? $data12[0]->dateecoute : "")?>',
            '<?php echo (isset($data12[1]) ? $data12[1]->dateecoute : "") ?>',
            '<?php echo (isset($data12[2]) ? $data12[2]->dateecoute : "") ?>',
            '<?php echo (isset($data12[3]) ? $data12[3]->dateecoute : "") ?>'
            ]
    },
    yAxis: {
        title: {
            text: "Nombre d'écoutes"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo (isset($data12[0]) ? $data12[0]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data12[1]) ? $data12[1]->nb : "0")?>
        }, {
            y:  <?php echo (isset($data12[2]) ? $data12[2]->nb : "0")?>
        }, {
            y: <?php echo (isset($data12[3]) ? $data12[3]->nb : "0")?>
        }]
    }]
});
</script>

<script>
    Highcharts.chart('pourcentEcoute', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Pourcentage d'écoute positives et négatives"
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    textOutline: false 
                }
            }
        }
    },
    series: [{
        name: 'Pourcentage',
        colorByPoint: true,
        data: [{
            name: 'Ecoutes positives',
            y: <?php echo ((!empty($data1))?(pourcentChart($data1)/(pourcentChart($data1)+pourcentChart($data2)))*100 : '0'); ?>
        }, {
            name: 'Ecoutes négatives',
            y: <?php echo ((!empty($data2))?(pourcentChart($data2)/(pourcentChart($data2)+pourcentChart($data1)))*100 : '0'); ?>
        }]
    }]
});
</script>

<script>
Highcharts.chart('nbEcoute', {
    chart: {
        backgroundColor: 'rgba(221,221,221,0.87)',
        height: 350,
        type: 'column'
    },
    credits: {
        enabled: false
    },

    title: {
        text: "Nombre d'écoute au total"
    },
    xAxis: {
        categories: ['Ecoutes positives', 'Ecoutes négatives', 'Ecoutes totales']
    },
    yAxis: {
        title: {
            text: "Nombre d'écoute"
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}',
                style: {
                    textOutline: false 
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">Nombre écoute </span>: <b>{point.y}</b> écoute(s)<br/>'
    },
    series: [{
        name: 'information:',
        colorByPoint: true,
        data: [{
            y: <?php echo (isset($data1) ? pourcentChart($data1) : "0")?>
        }, {
            y:  <?php echo (isset($data2) ? pourcentChart($data2) : "0")?>
        }, {
            y:  <?php echo (isset($data1)&&isset($data2) ? (pourcentChart($data1)+ pourcentChart($data2)) : "0")?>
        }]
    }]
});
</script>

<?php

function forWeek($data)
{
    for($i=0; $i<7; $i++)
    {
        $res[$i] = "0";
        $t = strtotime("monday")+(86400*$i);
        foreach ($data as $v) 
        {
            if(strtotime($v->dateecoute) == $t)
            {
                $res[$i] = $v->nb;
            }
        }   
    }
    return $res;
}


function pourcentChart($data)
{
    $sum = 0;
    foreach($data as $m)
    {
        $sum = $sum + $m->nb;
    }
    return $sum;
}


?>
<script type="text/javascript">
    $("#d1").css("left",$("li.item-test").last().offset().left)+"px";
    $("#d1").css("width",$("li.item-test").last().width())+5+"px";

    $("li.item-test").hover(function()
    {
        var thisitem = $(this);
        $("#d1").css("left", thisitem.offset().left);
        $("#d1").css("width", thisitem.width());
    });
</script>