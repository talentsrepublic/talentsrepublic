<?php
/* @var $this MusiqueController */
/* @var $data Musique */
?>

<div class="view">


	<b><?php echo CHtml::encode($data->getAttributeLabel('titre')); ?>:</b>
	<?php echo CHtml::encode($data->titre); ?>
	<br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('genre')); ?>:</b>
	<?php 
        foreach($data->genre as $item)
            echo CHtml::encode($item->libellegenre." "); 
        ?>
	<br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('auteur')); ?>:</b>
	<?php echo CHtml::encode($data->auteur->pseudo); ?>
	<br />


</div>