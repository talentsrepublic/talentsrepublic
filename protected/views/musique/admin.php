<?php
/* @var $this MusiqueController */
/* @var $model Musique */

$this->breadcrumbs=array(
	'Musiques'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Musique', 'url'=>array('index')),
	array('label'=>'Create Musique', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#musique-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="whitetext">
<h1>Manage Musiques</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'musique-grid',
        'htmlOptions'=>array('class'=>'table table-striped table-bordered table-hover'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'idmusique',
		'titre',
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{stat}',
                        'buttons'=>array
                           (
                               'stat' => array
                               (
                                   'label'=>'voir les stats',
                                   'url'=>'Yii::app()->createUrl("musique/statMusique", array("idmusique"=>$data->idmusique))',
                               )
                            )
		),
	),
)); ?>


<script type="text/javascript">
    $("#d1").css("left",$("li.item-test").last().offset().left)+"px";
    $("#d1").css("width",$("li.item-test").last().width())+5+"px";

    $("li.item-test").hover(function()
    {
        var thisitem = $(this);
        $("#d1").css("left", thisitem.offset().left);
        $("#d1").css("width", thisitem.width());
    });
</script>