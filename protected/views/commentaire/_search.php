<?php
/* @var $this CommentaireController */
/* @var $model Commentaire */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idcommentaire'); ?>
		<?php echo $form->textField($model,'idcommentaire',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idutilisateur'); ?>
		<?php echo $form->textField($model,'idutilisateur'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idpost'); ?>
		<?php echo $form->textField($model,'idpost'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contenu'); ?>
		<?php echo $form->textArea($model,'contenu',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->