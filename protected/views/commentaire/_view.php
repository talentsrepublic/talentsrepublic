<?php
/* @var $this CommentaireController */
/* @var $data Commentaire */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idcommentaire')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idcommentaire), array('view', 'id'=>$data->idcommentaire)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idutilisateur')); ?>:</b>
	<?php echo CHtml::encode($data->idutilisateur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idpost')); ?>:</b>
	<?php echo CHtml::encode($data->idpost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contenu')); ?>:</b>
	<?php echo CHtml::encode($data->contenu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />


</div>