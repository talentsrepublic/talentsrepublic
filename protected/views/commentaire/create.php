<?php
/* @var $this CommentaireController */
/* @var $model Commentaire */

$this->breadcrumbs=array(
	'Commentaires'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Commentaire', 'url'=>array('index')),
	array('label'=>'Manage Commentaire', 'url'=>array('admin')),
);
?>

<h1>Create Commentaire</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>