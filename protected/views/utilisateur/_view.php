<?php
/* @var $this UtilisateurController */
/* @var $data Utilisateur */
?>

<div class="view">
	<b><?php echo CHtml::encode($data->getAttributeLabel('idutilisateur')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idutilisateur), array('view', 'id'=>$data->idutilisateur)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prenom')); ?>:</b>
	<?php echo CHtml::encode($data->prenom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nom')); ?>:</b>
	<?php echo CHtml::encode($data->nom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pseudo')); ?>:</b>
	<?php echo CHtml::encode($data->pseudo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mail')); ?>:</b>
	<?php echo CHtml::encode($data->mail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('motdepasse')); ?>:</b>
	<?php echo CHtml::encode($data->motdepasse); ?>
	<br />
        


	<b><?php echo CHtml::encode($data->getAttributeLabel('villeresidence')); ?>:</b>
	<?php echo CHtml::encode($data->villeresidence); ?>
	<br />


</div>