<?php
/* @var $this UtilisateurController */
/* @var $model Utilisateur */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#utilisateur-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="whitetext">
<h1>Gérer les utilisateurs</h1>

<p>
Vous pouvez éventuellement utiliser les opérateurs de comparaisons(<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
ou <b>=</b>) au début de chacun de vos valeurs de recherches pour spécifier comment la comparaison doit se faire.
</p>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'utilisateur-grid',
        'htmlOptions'=>array('class'=>'table table-striped table-hover'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'idutilisateur',
		'prenom',
		'nom',
		'pseudo',
		'mail',
                'active',
		/*
		'villeresidence',
		*/
		array(
			'class'=>'CButtonColumn',
                        'template'=>'{valider}{invalider}{stat}',
                        'deleteConfirmation'=>"Etes-vous sûr de vouloir désactiver cet utilisateur ?",
                        'buttons'=>array
                        (
                            'valider' => array
                            (
                                'label'=>'valider',
                                'imageUrl'=>Yii::app()->theme->baseUrl.'/img/valide.png',
                                'url'=>'Yii::app()->createUrl("utilisateur/valider", array("id"=>$data->idutilisateur))',
                                'visible'=>'($data->active==0)'
                            ),
                            'invalider' => array
                            (
                                'label'=>'invalider',
                                'imageUrl'=>Yii::app()->theme->baseUrl.'/img/delete.png',
                                'url'=>'Yii::app()->createUrl("utilisateur/invalider", array("id"=>$data->idutilisateur))',
                                'visible'=>'($data->active==1)'
                            ),
                            'stat' => array
                            (
                                'label'=>'voir les stats',
                                'url'=>'Yii::app()->createUrl("utilisateur/statMusique", array("idutilisateur"=>$data->idutilisateur))',
                            )
                            
                        ),
		),
	),
)); ?>

</div>