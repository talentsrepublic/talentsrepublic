<?php
$baseUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript();
?>


<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/analyse.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/lecturemusique.js"></script>

<table class="content-user-table">
    <tr><td><div class="content-user">
                <?php
                /* @var $this UtilisateurController */
                /* @var $model Utilisateur */
                if ($model->active == 1) {
                    if (!Yii::app()->user->isGuest) {
                        if (Yii::app()->user->idutilisateur == $model->idutilisateur)
                            echo "<h1>$model->prenom $model->nom</h1>";
                        else
                            echo '<h1>' . $model->prenom . ' ' . $model->nom . '</h1>';
                    } else
                        echo '<h1>' . $model->prenom . ' ' . $model->nom . '</h1>';
                    ?>
                    <img width="120px" height="100px" style="border-radius: 50%" src="<?php echo Yii::app()->baseUrl . '/img/photosprofiles/' . $model->urlphoto; ?>">
                    <p>Prénom : <?php echo $model->prenom; ?></p>
                    <p>Nom : <?php echo $model->nom; ?></p>
                    <p>Pseudo : <?php echo $model->pseudo; ?></p>
                    <p>Adresse mail : <?php echo $model->mail; ?></p>
                    <?php
                    if (!Yii::app()->user->isGuest) {
                        if ($model->idutilisateur != Yii::app()->user->idutilisateur) {
                            echo CHtml::ajaxSubmitButton((!$estdejaamis) ? ($invitationenvoyee ? 'Demande d\'ajout envoyée !' : 'Ajouter en amis') : 'Vous êtes déjà amis', array('utilisateur/ajouterami'), array('data' => array('idami' => $model->idutilisateur),
                                'success' => 'function(data){       var res = JSON.parse(data);
                                                if(res.status)
                                                {
                                                    $("#btn_ajouterenami").val(res.message);
                                                    $("#btn_ajouterenami").attr("disabled","true");
                                                    $("#btn_supprimerami").show();
                                                }
                                                else
                                                    alert(res.message);

                                            }',
                                    ), array('id' => 'btn_ajouterenami', 'class' => 'btn', 'disabled' => ($estdejaamis || $invitationenvoyee)));

                            //Bouton supprimer en amis
                            //Si l'invitation est envoyée (que l'autre ait répondu ou non)
                            echo CHtml::ajaxSubmitButton(($estdejaamis) ? 'Retirer de ma liste d\'amis' : 'Annuler l\'invitation', array('utilisateur/supprimerami'), array('data' => array('idami' => $model->idutilisateur),
                                'success' => 'function(data){                                             
                                                    var res = JSON.parse(data);
                                                    alert(res.message);
                                                    if(res.status)
                                                    {
                                                        $("#btn_ajouterenami").val("Ajouter en amis");
                                                        $("#btn_ajouterenami").removeAttr("disabled");
                                                        $("#btn_supprimerami").hide();
                                                    }

                                                }'
                                    ), array('id' => 'btn_supprimerami', 'class' => 'btn btn-danger', 'style' => (!$invitationenvoyee) ? 'display:none' : ''));
                        }
                    }
                } else {
                    echo "<h1>Ce compte a été désactivé !</h1>";
                }
                ?>
            </div>
        </td>
        <td class="user-musique">
            <div id="righthandside" class="music-user-wrapper">
                <table id="tablemusiques" class="tablesorter table table-striped table-bordered table-hover">
                    <thead>
                    <th class="thtitre">Titre</th>
                    <th class="thdate">Date</th>
                    <th class="thalbum">Album</th>
                    <th class="thgenres">Genre(s)</th>
                    <th class="thsuppr"></th>
                    </thead>
                    <?php
                    $i = 0;
                    if (empty($messons)) {
                        echo "<h2>C'est calme par ici...</h2>";
                    } else {
                        foreach ($messons as $musique) {
                            echo "<tr class='getId' idmusique='$musique->idmusique'>";
                            echo "<td>";
                            echo '<div class="jp-controls musique" titre="' . $musique->titre . '" url="/musique/' . $musique->urlmusique . '">
                                    <button class="jp-play" onclick="getDureeMusique();loadMusique(this);loadStat();getIdMusique(this);" role="button" tabindex="0">play</button>
                                    <button class="jp-stop" onclick="stop();stopStat();" role="button" tabindex="0">stop</button>
                                </div>';
                            echo "$musique->titre<div id='audiospectrum$i' class='audiospec'></div></td>";
                            echo "<td>$musique->dateajout</td>";
                            if (isset($musique->libellealbum))
                                echo "<td>$musique->libellealbum</td>";
                            else
                                echo "<td>Sans Album</td>";
                            if (isset($musique->genre)) {
                                echo "<td>";
                                foreach ($musique->genre as $genre) {
                                    echo "$genre->libellegenre ";
                                }
                                echo "</td>";
                            } else
                                echo "<td>Aucun genre</td>";
                            echo "<td>";

                            if (!Yii::app()->user->isGuest) {

                                if ($musique->idutilisateur == Yii::app()->user->idutilisateur) {

                                    echo CHtml::ajaxSubmitButton(
                                            'Delete', Yii::app()->createUrl('musique/delete', array("id" => $musique->idmusique)), array(
                                        'type' => 'POST',
                                        'success' => 'function(){ $("tr[id=' . $musique->idmusique . ']").remove();}'
                                    ));
                                }
                            }
                            echo "</td>";
                            echo '</tr>';
                            $i++;
                        }
                    }
                    ?>
                </table>
            </div>
        </td></tr>    
</table>

<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/tablesorter/jquery.tablesorter.min.js"></script> 
<script type="text/javascript">
    function resize_tab()//resize le tableau de droite avec les resize de page
    {
        var nwidth = $(window).width();
        nwidth -= parseInt(($(".content-user").outerWidth(true)));
        nwidth -= 100;
        $("#righthandside").css("width", nwidth);
    }
    $(document).ready(function () {
        $this = $('#righthandside');
        var sum = 20;//pas notable quand la différence est inférieure à 20 ? (à vérifier)
        $this.children().each(function () {
            sum += parseInt($(this).css("height"));
        });
        if (parseInt($this.css("height")) < sum)
        {
            $this.css("max-height", ($("#content").height()));
            $this.css("overflow", "hidden");
            $this.mCustomScrollbar({scrollInertia: 10});
        }
        resize_tab();
        $("#tablemusiques").tablesorter();
    });
    window.onresize = function () {
        resize_tab();
    };



</script>

