<?php
/* @var $this UtilisateurController */
/* @var $model Utilisateur */

$this->breadcrumbs=array(
	'Utilisateurs'=>array('index'),
	$model->idutilisateur=>array('view','id'=>$model->idutilisateur),
	'Update',
);

$this->menu=array(
	array('label'=>'List Utilisateur', 'url'=>array('index')),
	array('label'=>'Create Utilisateur', 'url'=>array('create')),
	array('label'=>'View Utilisateur', 'url'=>array('view', 'id'=>$model->idutilisateur)),
	array('label'=>'Manage Utilisateur', 'url'=>array('admin')),
);
?>

<h1>Profil de <?php echo $model->prenom.' '.$model->nom; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>