<?php
/* @var $this AlbumController */
/* @var $data Album */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idalbum')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idalbum), array('view', 'id'=>$data->idalbum)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('libellealbum')); ?>:</b>
	<?php echo CHtml::encode($data->libellealbum); ?>
	<br />


</div>