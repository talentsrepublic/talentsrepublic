<?php
/* @var $this AlbumController */
/* @var $model Album */

?>
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
    L'album a été ajouté !
</div>
<h1>Album <?php echo $model->libellealbum; ?></h1>

<table id="tablemusiques" class="tablesorter table table-striped table-bordered table-hover">
                <thead>
                        <th class="thtitre">Titre</th>
                        <th class="thdate">Date</th>
                        <th class="thalbum">Album</th>
                        <th class="thsuppr"></th>
                </thead>
            <?php
                $i=0;
                    foreach($model->musiques as $musique)
                    {
                        echo "<tr id='$musique->idmusique'>";
                        echo "<td>";
                        echo '<div class="jp-controls musique" titre="'.$musique->titre.'" url="/musique/'.$musique->urlmusique.'">
                                    <button class="jp-play" onclick="loadMusique(this);loadStat();" role="button" tabindex="0">play</button>
                                    <button class="jp-stop" onclick="stop();stopStat();" role="button" tabindex="0">stop</button>
                                </div>';
                        echo "$musique->titre<div id='audiospectrum$i' class='audiospec'></div></td>";
                        echo "<td>$musique->dateajout</td>";
                        echo "<td>$model->libellealbum</td>";
                        echo "<td>";
                        echo CHtml::ajaxSubmitButton(
                                    'Delete', Yii::app()->createUrl('musique/delete',array("id"=>$musique->idmusique)),array(
                                    'type' => 'POST',
                                    'success' => 'function(){ $("tr[id='.$musique->idmusique.']").remove();}'
                )
        ); 
                        echo "</td>";
                        echo '</tr>';
                        
                        $i++;
                    }
            ?>
            </table>

    <script type="text/javascript" src="<?php echo $baseUrl;?>/js/tablesorter/jquery.tablesorter.min.js"></script> 